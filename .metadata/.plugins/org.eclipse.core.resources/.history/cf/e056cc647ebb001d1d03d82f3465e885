package com.cms.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.dao.BranchDao;
import com.cms.entities.BranchEntity;
import com.cms.entities.CourierEntity;
import com.cms.entities.StatusEnum;

@Service
@Transactional
public class BranchServiceImpl implements BranchService {

	@Autowired
	private BranchDao branchDao;
	
	@Override
	public List<BranchEntity> findAllBranches() {

		return branchDao.findAll();
	}

	@Override
	public BranchEntity addBranch(BranchEntity branch) {
		return branchDao.save(branch);
	}

	@Override
	public BranchEntity updateBranch(BranchEntity branchtobeupdated) {
		BranchEntity updatedBranch = branchDao.save(branchtobeupdated);
		return updatedBranch;
	}

	@Override
	public void deleteBranch(Long bid) {
		branchDao.deleteById(bid);
	}

	@Override
	public BranchEntity getBranch(Long branch) {
		return branchDao.findById(branch).get();
	}

	@Override
	public BranchEntity findByPIn(String pincode) {
 		return branchDao.findByBranchAddressPincode(pincode);
 		}

	@Override
	public List<CourierEntity> getAllPickedUpCouriersOfBranch(Long branchid) {
		  BranchEntity branch =branchDao.findById(branchid).get();
		 List<CourierEntity> list = branch.getAllCouriers()
				 .stream()
				 .filter(s->s.getCourierStatus()==StatusEnum.valueOf("PICKED_UP"))
				 .collect(Collectors.toList());
		return list;
	}

	@Override
	public List<CourierEntity> getAllIntransitCouriersOfBranch(Long branchid) {
		BranchEntity branch =branchDao.findById(branchid).get();
		List<CourierEntity> list = branch.getAllCouriers()
				.stream()
				.filter(s->s.getCourierStatus()==StatusEnum.valueOf("INTRANSIT"))
				.collect(Collectors.toList());
		return list;
		
 	}

	@Override
	public Map<String, Boolean> checkAvailability(String spin, String rpin) {
		BranchEntity sbranch = branchDao.findByBranchAddressPincode(spin);
		BranchEntity rbranch = branchDao.findByBranchAddressPincode(rpin);
		Map<String,Boolean> map=new LinkedHashMap<>();
 		if(sbranch!=null) {
 			map.put(spin, true); 			
 		}
 		if(rbranch!=null) {
 			map.put(rpin, true); 			
 		}
 		return map;
 		
	}
//------------------------get branch by branch id-------------------------
	@Override
	public BranchEntity getBranchByBranchId(Long branchid) {
		
		return branchDao.findById(branchid).get();
	}
  
}
